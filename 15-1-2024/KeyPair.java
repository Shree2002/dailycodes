class Solution {
    boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        // code here
        
        HashSet<Integer> map =new HashSet<Integer>();
        
        for(int i=0;i<n;i++){
            if(map.contains(x-arr[i])){
                return true;
            }
            else{
                map.add(arr[i]);
            }
        }
        return false;
    }
}


//finding the complementary element in the hashmap if present then it is true otherwise false