class Solution {
    public int singleNumber(int[] nums) {
        HashMap<Integer,Integer>mp = new LinkedHashMap<>();

         for (int i = 0; i < nums.length; i++) {
            mp.put(nums[i], mp.getOrDefault(nums[i], 0) + 1);
        }

            for(Map.Entry<Integer,Integer>it:mp.entrySet()){
                if(it.getValue() == 1){
                    return it.getKey();
                }
            }


        return -1;
    }
}

//https://leetcode.com/problems/single-number/solutions/4571066/brute-force-approach-in-java-using-hasmap/