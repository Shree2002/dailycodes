class Solution {
    // Function to return the position of the first repeating element.
    public static int firstRepeated(int[] arr, int n) {
        // Your code here
        
HashMap<Integer,Integer> hm = new HashMap<>();
     int temp;
     for(int i=0;i<n;i++){
          temp=0;
        
        if(hm.containsKey(arr[i])){
            temp=hm.get(arr[i])+1;
        } else{
            temp=1;
        }
        
        hm.put(arr[i],temp);
         
     }
     for(int i = 0;i<n;i++){
         if(hm.get(arr[i])>1)
             return i+1;
     }
     return -1;
        
    }
}


//hasmap , used for strting the frequency of elements in the array
// ans returning the element(key) if its value is greater than 1